package com.fundtransfer.test.services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import org.apache.http.client.HttpClient;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fundtransfer.dao.DAOFactory;
import com.fundtransfer.service.AccountService;
import com.fundtransfer.service.ServiceExceptionMapper;
import com.fundtransfer.service.TransactionService;
import com.fundtransfer.utils.FundTrnasferConstants;


public abstract class TestService {
	protected static Server server = null;
	protected static PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();

	protected static HttpClient client ;
	protected static DAOFactory h2DaoFactory =
			DAOFactory.getDAOFactory();
	protected ObjectMapper mapper = new ObjectMapper();
	protected URIBuilder builder = new URIBuilder().setScheme("http").setHost("localhost:8084");


	@BeforeClass
	public static void setup() throws Exception {
		h2DaoFactory.populateTestData(FundTrnasferConstants.SQL_FILE_NAME);
		startServer();
		connManager.setDefaultMaxPerRoute(100);
		connManager.setMaxTotal(200);
		client= HttpClients.custom()
				.setConnectionManager(connManager)
				.setConnectionManagerShared(true)
				.build();

	}

	@AfterClass
	public static void closeClient() throws Exception {
		//server.stop();
		HttpClientUtils.closeQuietly(client);
		// To remove the H2 DB data
		h2DaoFactory.removeTestData(FundTrnasferConstants.SQL_DELETE_FILE_NAME);
	}

	/**
	 * To start server
	 * @throws Exception
	 */
	private static void startServer() throws Exception {
		if (server == null) {
			server = new Server(8084);
			ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
			context.setContextPath("/");
			server.setHandler(context);
			ServletHolder servletHolder = context.addServlet(ServletContainer.class, "/*");
			servletHolder.setInitParameter("jersey.config.server.provider.classnames",
					AccountService.class.getCanonicalName() + "," +
							ServiceExceptionMapper.class.getCanonicalName() + "," +
							TransactionService.class.getCanonicalName());
			server.start();
		}
	}
}
