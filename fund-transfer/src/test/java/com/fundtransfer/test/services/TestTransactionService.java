package com.fundtransfer.test.services;


import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import com.fundtransfer.model.UserTransaction;

/**
 * Integration testing for RestAPI
 * Test data are initialised from src/test/resources/demo.sql
 * <p>
 */
public class TestTransactionService extends TestService {
	//test transaction related operations in the account

	/*
       TC B4 Positive Category = AccountService
       Scenario: test transaction from one account to another with source account has sufficient fund
                 return 200 OK
	 */
	@Test
	public void testTransactionEnoughFund() throws IOException, URISyntaxException {
		URI uri = builder.setPath("/transaction").build();
		BigDecimal amount = new BigDecimal(10).setScale(4, RoundingMode.HALF_EVEN);
		UserTransaction transaction = new UserTransaction("EUR", amount, 3L, 4L, "31-05-18 18:34:24");

		String jsonInString = mapper.writeValueAsString(transaction);
		StringEntity entity = new StringEntity(jsonInString);
		HttpPost request = new HttpPost(uri);
		request.setHeader("Content-type", "application/json");
		request.setEntity(entity);
		HttpResponse response = client.execute(request);
		int statusCode = response.getStatusLine().getStatusCode();
		assertTrue(statusCode == 200);
	}

	/*
        TC B5 Negative Category = AccountService
        Scenario: test transaction from one account to another with source account has no sufficient fund
                  return 500 INTERNAL SERVER ERROR
	 */
	@Test
	public void testTransactionNotEnoughFund() throws IOException, URISyntaxException {
		URI uri = builder.setPath("/transaction").build();
		BigDecimal amount = new BigDecimal(100000).setScale(4, RoundingMode.HALF_EVEN);
		UserTransaction transaction = new UserTransaction("EUR", amount, 3L, 4L, "31-05-13 11:34:24");

		String jsonInString = mapper.writeValueAsString(transaction);
		StringEntity entity = new StringEntity(jsonInString);
		HttpPost request = new HttpPost(uri);
		request.setHeader("Content-type", "application/json");
		request.setEntity(entity);
		HttpResponse response = client.execute(request);
		int statusCode = response.getStatusLine().getStatusCode();
		assertTrue(statusCode == 500);
	}

	/*
    TC B5 Negative Category = AccountService
    Scenario: test transaction from one account to another with source account has no sufficient fund
              return 500 INTERNAL SERVER ERROR
	 */
	@Test
	public void testTransactionInvalidAccounts() throws IOException, URISyntaxException {
		URI uri = builder.setPath("/transaction").build();
		BigDecimal amount = new BigDecimal(100000).setScale(4, RoundingMode.HALF_EVEN);
		UserTransaction transaction = new UserTransaction("EUR", amount, null, null, "31-05-13 11:34:24");

		String jsonInString = mapper.writeValueAsString(transaction);
		StringEntity entity = new StringEntity(jsonInString);
		HttpPost request = new HttpPost(uri);
		request.setHeader("Content-type", "application/json");
		request.setEntity(entity);
		HttpResponse response = client.execute(request);
		int statusCode = response.getStatusLine().getStatusCode();
		assertTrue(statusCode == 500);
	}
	/*
       TC C1 Negative Category = TransactionService
       Scenario: test transaction from one account to another with source/destination account with different currency code
                 return 500 INTERNAL SERVER ERROR
	 */
	@Test
	public void testTransactionDifferentCcy() throws IOException, URISyntaxException {
		URI uri = builder.setPath("/transaction").build();
		BigDecimal amount = new BigDecimal(100).setScale(4, RoundingMode.HALF_EVEN);
		UserTransaction transaction = new UserTransaction("USD", amount, 3L, 4L, "31-05-13 11:34:24");

		String jsonInString = mapper.writeValueAsString(transaction);
		StringEntity entity = new StringEntity(jsonInString);
		HttpPost request = new HttpPost(uri);
		request.setHeader("Content-type", "application/json");
		request.setEntity(entity);
		HttpResponse response = client.execute(request);
		int statusCode = response.getStatusLine().getStatusCode();
		assertTrue(statusCode == 500);
	}
	/*
    TC A1 Positive Category = TransactionService
    Scenario: test get transaction by transactionId
              return 200 OK
	 */
	@Test
	public void testGetTransactionById() throws IOException, URISyntaxException {
		URI uri = builder.setPath("/transaction/20181031233045").build();
		HttpGet request = new HttpGet(uri);
		HttpResponse response = client.execute(request);
		int statusCode = response.getStatusLine().getStatusCode();
		assertTrue(statusCode == 200);
		String jsonString = EntityUtils.toString(response.getEntity());
		assertTrue(jsonString.contains("SUCCESS"));
	}
	/*
    TC A1 Negative Category = TransactionService
    Scenario: test get transaction by transactionId null
              return 404 OK
	 */
	@Test
	public void testGetTransactionByIdInvalidId() throws IOException, URISyntaxException {
		URI uri = builder.setPath("/transaction/null").build();
		HttpGet request = new HttpGet(uri);
		HttpResponse response = client.execute(request);
		int statusCode = response.getStatusLine().getStatusCode();
		assertTrue(statusCode == 404);
	}
}