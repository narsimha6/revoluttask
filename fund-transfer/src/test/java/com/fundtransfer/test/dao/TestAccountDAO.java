package com.fundtransfer.test.dao;

import static junit.framework.TestCase.assertTrue;

import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.List;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fundtransfer.dao.DAOFactory;
import com.fundtransfer.exception.CustomException;
import com.fundtransfer.model.Account;
import com.fundtransfer.model.UserTransaction;
import com.fundtransfer.utils.FundTrnasferConstants;

public class TestAccountDAO {

	private static final DAOFactory h2DaoFactory = DAOFactory.getDAOFactory();

	@BeforeClass
	public static void setup() {
		// prepare test database and test data. Test data are initialised from
		// src/test/resources/demo.sql
		h2DaoFactory.populateTestData(FundTrnasferConstants.SQL_FILE_NAME);
	}

	@After
	public void tearDown() {

	}

	@Test(expected= RuntimeException.class)
	public void testPopulateTestData() {
		h2DaoFactory.populateTestData(FundTrnasferConstants.SQL_FILE_NAME+"1");;
	}
	
	@Test
	public void testGetAllAccounts() throws CustomException {
		List<Account> allAccounts = h2DaoFactory.getAccountDAO().getAllAccounts();
		assertTrue(allAccounts.size() > 1);
	}

	@Test
	public void testGetAccountById() throws CustomException {
		Account account = h2DaoFactory.getAccountDAO().getAccountById(1L);
		assertTrue(account.getUserName().equals("Fred"));
	}

	@Test
	public void testGetNonExistingAccById() throws CustomException {
		Account account = h2DaoFactory.getAccountDAO().getAccountById(100L);
		assertTrue(account == null);
	}
	
	@Test
	public void testGetTransactionById() throws CustomException {
		String res = h2DaoFactory.getAccountDAO().getTransactionById(20181031233045L);
		assertTrue(res.equals("SUCCESS - null"));
	}
	
	@Test(expected=CustomException.class)
	public void testTransferAccountBalanceByInvalidAccountId() throws CustomException {
		UserTransaction userTransaction = new UserTransaction("EUR", new BigDecimal("100.000"), null, null, "31-05-13 11:34:24");
		h2DaoFactory.getAccountDAO().transferAccountBalance(userTransaction);
	}
	
	@Test(expected=CustomException.class)
	public void testTransferAccountBalanceByInvalidObject() throws CustomException {
		h2DaoFactory.getAccountDAO().transferAccountBalance(null);
	}
}