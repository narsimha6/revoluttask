--This script is used for unit test cases, DO NOT CHANGE!

DROP TABLE IF EXISTS Account;

CREATE TABLE Account (AccountId LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
UserName VARCHAR(30),
Balance DECIMAL(19,4),
CurrencyCode VARCHAR(30),
TxTime TIMESTAMP DEFAULT CURRENT_TIMESTAMP 
);

CREATE UNIQUE INDEX idx_acc on Account(UserName,CurrencyCode);

INSERT INTO Account (UserName,Balance,CurrencyCode, TxTime) VALUES ('Fred',1000.0000,'USD', '2018-10-31 18:47:52.69');
INSERT INTO Account (UserName,Balance,CurrencyCode, TxTime) VALUES ('Riad',200.0000,'USD', '2018-10-31 18:47:52.69');
INSERT INTO Account (UserName,Balance,CurrencyCode, TxTime) VALUES ('Venkat',500.0000,'EUR', '2018-10-31 18:47:52.69');
INSERT INTO Account (UserName,Balance,CurrencyCode, TxTime) VALUES ('Baldung',500.0000,'EUR', '2018-10-31 18:47:52.69');
INSERT INTO Account (UserName,Balance,CurrencyCode, TxTime) VALUES ('Edward',500.0000,'GBP', '2018-10-31 18:47:52.69');
INSERT INTO Account (UserName,Balance,CurrencyCode, TxTime) VALUES ('Narasimha',500.0000,'GBP', '2018-10-31 18:47:52.69');

--This script is used for unit test cases, DO NOT CHANGE!

DROP TABLE IF EXISTS BankTransaction;

CREATE TABLE BankTransaction (TxId LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
fromAccountId LONG not null,
toAccountId LONG not null,
Amount DECIMAL(19,4),
TxType VARCHAR(20),
CurrencyCode VARCHAR(30),
Status VARCHAR(30),
ErrorMsg VARCHAR(1000),
TxTime TIMESTAMP DEFAULT CURRENT_TIMESTAMP 
);

CREATE UNIQUE INDEX idx_BankTransaction on BankTransaction(TxId, fromAccountId, toAccountId);

INSERT INTO BankTransaction (TxId,fromAccountId,toAccountId, Amount, TxType, CurrencyCode, Status, ErrorMsg, TxTime) 
VALUES (20181031233045, 1,2,50.0000, 'SWIFT', 'USD', 'SUCCESS', null, '2018-10-31 18:47:52.69');

