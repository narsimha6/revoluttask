package com.fundtransfer.dao.impl;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.commons.dbutils.DbUtils;
import org.apache.log4j.Logger;

import com.fundtransfer.dao.AccountDAO;
import com.fundtransfer.dao.H2DAOFactory;
import com.fundtransfer.exception.CustomException;
import com.fundtransfer.model.Account;
import com.fundtransfer.model.MoneyUtil;
import com.fundtransfer.model.UserTransaction;
import com.fundtransfer.utils.FundTrnasferConstants;

public class AccountDAOImpl implements AccountDAO {

	private static Logger log = Logger.getLogger(AccountDAOImpl.class);

	/**
	 * Get all accounts.
	 */
	public List<Account> getAllAccounts() throws CustomException {
		List<Account> allAccounts = new ArrayList<Account>();
		try(Connection conn = H2DAOFactory.getConnection();
				PreparedStatement stmt = conn.prepareStatement(FundTrnasferConstants.SQL_GET_ALL_ACC);
				ResultSet rs = stmt.executeQuery();) {
			while (rs.next()) {
				Account acc = new Account(rs.getLong("AccountId"), rs.getString("UserName"),
						rs.getBigDecimal("Balance"), rs.getString("CurrencyCode"));
				if (log.isDebugEnabled())
					log.debug("getAllAccounts(): Get  Account " + acc);
				allAccounts.add(acc);
			}
			return allAccounts;
		} catch (SQLException e) {
			throw new CustomException("getAccountById(): Error reading account data", e, 1001L);
		} 
	}

	/**
	 * Get account by id
	 */
	public Account getAccountById(Long accountId) throws CustomException {
		Account acc = null;
		ResultSet rs = null;
		try (Connection conn = H2DAOFactory.getConnection();
				PreparedStatement stmt = conn.prepareStatement(FundTrnasferConstants.SQL_GET_ACC_BY_ID);) {
			stmt.setObject(1, accountId);
			rs = stmt.executeQuery();
			if (rs.next()) {
				acc = new Account(rs.getLong("AccountId"), rs.getString("UserName"), rs.getBigDecimal("Balance"),
						rs.getString("CurrencyCode"));
				if (log.isDebugEnabled())
					log.debug("Retrieve Account By Id: " + acc);
			}
			return acc;
		} catch (SQLException e) {
			throw new CustomException("getAccountById(): Error reading account data", e, 1002L);
		} finally {
			DbUtils.closeQuietly( rs);
		}
	}

	/***
	 * Transfer balance between two accounts.
	 * @param userTransaction
	 * @return long
	 * @exception CustomException
	 */
	public String transferAccountBalance(UserTransaction userTransaction) throws CustomException {
		if(Objects.isNull(userTransaction) || (Objects.isNull(userTransaction.getFromAccountId()) 
				|| Objects.isNull(userTransaction.getToAccountId() ))){
			throw new CustomException("From and to accounts IDs must not be NULL", 1003L);
		}
		int result = -1;
		Connection conn = null;
		PreparedStatement lockStmt = null;
		PreparedStatement updateStmt = null;
		ResultSet rs = null;
		Account fromAccount = null;
		Account toAccount = null;
		String status = null;
		String errorMsg = null; 
		String txId = null;
		try {
			conn = H2DAOFactory.getConnection();
			conn.setAutoCommit(false);
			// lock the credit and debit account for writing:
			lockStmt = conn.prepareStatement(FundTrnasferConstants.SQL_LOCK_ACC_BY_ID);
			lockStmt.setLong(1, userTransaction.getFromAccountId());
			rs = lockStmt.executeQuery();
			if (rs.next()) {
				fromAccount = new Account(rs.getLong("AccountId"), rs.getString("UserName"),
						rs.getBigDecimal("Balance"), rs.getString("CurrencyCode"));
				if (log.isDebugEnabled())
					log.debug("transferAccountBalance from Account: " + fromAccount);
			}
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(lockStmt);
			lockStmt = conn.prepareStatement(FundTrnasferConstants.SQL_LOCK_ACC_BY_ID);
			lockStmt.setObject(1, userTransaction.getToAccountId());
			rs = lockStmt.executeQuery();
			if (rs.next()) {
				toAccount = new Account(rs.getLong("AccountId"), rs.getString("UserName"), rs.getBigDecimal("Balance"),
						rs.getString("CurrencyCode"));
				if (log.isDebugEnabled())
					log.debug("transferAccountBalance to Account: " + toAccount);
			}
			
			// Business validation before transfer funds
			BigDecimal fromAccountLeftOver = validateTransaction(fromAccount, toAccount, userTransaction, status, errorMsg);
			
			// proceed with update
			updateStmt = conn.prepareStatement(FundTrnasferConstants.SQL_UPDATE_ACC_BALANCE);
			updateStmt.setObject(1, fromAccountLeftOver);
			updateStmt.setObject(2, userTransaction.getFromAccountId());
			updateStmt.addBatch();
			updateStmt.setObject(1, toAccount.getBalance().add(userTransaction.getAmount()));
			updateStmt.setObject(2, userTransaction.getToAccountId());
			updateStmt.addBatch();
			int[] rowsUpdated = updateStmt.executeBatch();
			result = rowsUpdated[0] + rowsUpdated[1];
			if (log.isDebugEnabled()) {
				log.debug("Number of rows updated for the transfer : " + result);
			}
			// If there is no error, commit the transaction
			conn.commit();
			status = FundTrnasferConstants.TX_SUCCESS;
		} catch (SQLException se) {
			// rollback transaction if exception occurs
			log.error("transferAccountBalance(): User Transaction Failed, rollback initiated for: " + userTransaction,
					se);
			try {
				if (conn != null)
					conn.rollback();
			} catch (SQLException re) {
				errorMsg = "Fail to rollback transaction";
				status = FundTrnasferConstants.TX_FAIL;
				throw new CustomException(errorMsg, re, 1003L);
			}
		} finally {
			txId = insertTransaction(userTransaction, status, errorMsg);
			DbUtils.closeQuietly(conn);
			DbUtils.closeQuietly(rs);
			DbUtils.closeQuietly(lockStmt);
			DbUtils.closeQuietly(updateStmt);
		}
		return txId;
	}
	
	/***
	 * Business validations check before transfer
	 * @param fromAccount
	 * @param toAccount
	 * @param userTransaction
	 * @param status
	 * @param errorMsg
	 * @return
	 * @throws CustomException
	 */
	private BigDecimal validateTransaction(Account fromAccount,Account toAccount
			, UserTransaction userTransaction, String status, String errorMsg) 
					throws CustomException {
		if(Objects.isNull(fromAccount) || Objects.isNull(toAccount) || Objects.isNull(userTransaction)) {
			errorMsg = "Invalid params for validating Transaction";
			status = FundTrnasferConstants.TX_FAIL;
			throw new CustomException(errorMsg);
		}
		// check locking status
		if (fromAccount == null || toAccount == null) {
			errorMsg = "Fail to lock both accounts for write";
			status = FundTrnasferConstants.TX_FAIL;
			throw new CustomException(errorMsg);
		}

		// check transaction currency
		if (!fromAccount.getCurrencyCode().equals(userTransaction.getCurrencyCode())) {
			errorMsg = "Fail to transfer Fund, transaction ccy are different from source/destination";
			status = FundTrnasferConstants.TX_FAIL;
			throw new CustomException(errorMsg);
		}

		// check ccy is the same for both accounts
		if (!fromAccount.getCurrencyCode().equals(toAccount.getCurrencyCode())) {
			errorMsg = "Fail to transfer Fund, the source and destination account are in different currency";
			status = FundTrnasferConstants.TX_FAIL;
			throw new CustomException(errorMsg);
		}

		// check enough fund in source account
		BigDecimal fromAccountLeftOver = fromAccount.getBalance().subtract(userTransaction.getAmount());
		if (fromAccountLeftOver.compareTo(MoneyUtil.zeroAmount) < 0) {
			errorMsg = "Not enough Fund from source Account ";
			status = FundTrnasferConstants.TX_FAIL;
			throw new CustomException(errorMsg);
		}
		return fromAccountLeftOver;
	}

	/***
	 * Inserting the each executed transaction in to DB
	 * @param userTransaction
	 * @param status
	 * @param errorMsg
	 * @return
	 * @throws CustomException 
	 */
	private String insertTransaction(UserTransaction userTransaction, String status, String errorMsg) 
			throws CustomException {
		if (Objects.isNull(userTransaction)) {
			throw new CustomException("Transaction Cannot be recorded, param Object can not be NULL.");
		}
		ResultSet generatedKeys = null;
		try (Connection conn = H2DAOFactory.getConnection();
				PreparedStatement stmt = conn.prepareStatement(FundTrnasferConstants.SQL_INSERT_BANK_TRANSACTION);){
			java.sql.Timestamp  txTime = new java.sql.Timestamp(new java.util.Date().getTime());
			stmt.setObject(1, txTime.getTime());
			stmt.setObject(2, userTransaction.getFromAccountId());
			stmt.setObject(3, userTransaction.getToAccountId());
			stmt.setObject(4, userTransaction.getAmount());
			stmt.setObject(5, "SWFIT/ONLINE");// Transfer type like Swift/NTFS/IMPS
			stmt.setObject(6, userTransaction.getCurrencyCode());
			stmt.setObject(7, status);
			stmt.setObject(8, errorMsg);
			stmt.setTimestamp(9, txTime);
			int affectedRows = stmt.executeUpdate();
			if (affectedRows == 0) {
				log.error("insertTransaction(): inserting transaction failed, no rows affected.");
				throw new CustomException("Transaction Cannot be recorded");
			}
			generatedKeys = stmt.getGeneratedKeys();
			if (!Objects.isNull(txTime.getTime())) {
				return txTime.getTime() +" - "+status;
			} else {
				log.error("Recording transaction failed, no ID obtained.");
				throw new CustomException("Transaction Cannot be created");
			}
		} catch (SQLException e) {
			log.error("Error while Inserting Transaction  " + userTransaction.toString());
			throw new CustomException("insertTransaction(): Error creating user transaction " +  userTransaction.toString(), e, 1004L);
		} finally {
			DbUtils.closeQuietly(generatedKeys);
		}
	}

	/***
	 * Get transaction from BANK_TRANSACTION table
	 * @param transactionId
	 */
	@Override
	public String getTransactionById(Long transactionId) throws CustomException {
		if(Objects.isNull(transactionId)) {
			throw new CustomException("getTransactionById(): TransactionId cannot be NULL.");
		}
		String result = null;
		ResultSet rs = null;
		try (Connection conn = H2DAOFactory.getConnection();
				PreparedStatement stmt = conn.prepareStatement(FundTrnasferConstants.SQL_GET_TX_BY_ID);
				) {
			stmt.setLong(1, transactionId);
			rs = stmt.executeQuery();
			if (rs.next()) {
				result = rs.getString("Status") + " - " + rs.getString("ErrorMsg");
				if (log.isDebugEnabled())
					log.debug("Retrieve transaction By Id: " + transactionId);
			}
			return result;
		} catch (SQLException e) {
			throw new CustomException("getTransactionById(): Error reading transaction data", e, 1005L);
		} finally {
			DbUtils.closeQuietly( rs);
		}
	}
}