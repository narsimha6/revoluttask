package com.fundtransfer.dao;

import java.util.List;

import com.fundtransfer.exception.CustomException;
import com.fundtransfer.model.Account;
import com.fundtransfer.model.UserTransaction;


public interface AccountDAO {

    List<Account> getAllAccounts() throws CustomException;
    Account getAccountById(Long accountId) throws CustomException;
    String getTransactionById(Long transactionId) throws CustomException;

    String transferAccountBalance(UserTransaction userTransaction) throws CustomException;
}
