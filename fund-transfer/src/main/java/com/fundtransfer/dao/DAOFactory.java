package com.fundtransfer.dao;

public interface DAOFactory {

	public abstract AccountDAO getAccountDAO();

	public abstract void populateTestData(String fileName);
	public void removeTestData(String fileName);

	public static DAOFactory getDAOFactory() {
		// by default using H2 in memory database
		return new H2DAOFactory();
	}
}
