package com.fundtransfer.dao;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;
import org.apache.log4j.Logger;
import org.h2.tools.RunScript;

import com.fundtransfer.dao.impl.AccountDAOImpl;
import com.fundtransfer.utils.FundTrnasferConstants;

/**
 * H2 DAO
 * @author Jillella Narasimha Rao
 */
public class H2DAOFactory implements DAOFactory {
	
	private static Logger log = Logger.getLogger(H2DAOFactory.class);

	private AccountDAO accountDAO = null;

	H2DAOFactory() {
		// init: load driver
		DbUtils.loadDriver(FundTrnasferConstants.H2_DRIVER);
	}

	public static Connection getConnection() throws SQLException {
		return DriverManager.getConnection(FundTrnasferConstants.H2_CONNECTION_URL, FundTrnasferConstants.H2_USER, FundTrnasferConstants.H2_PASSWORD);
	}

	public AccountDAO getAccountDAO() {
		accountDAO = new AccountDAOImpl();
		return accountDAO;
	}

	@Override
	public void populateTestData(String fileName) {
		log.info("Populating Test User Table and data ..... ");
		try(Connection conn = H2DAOFactory.getConnection();) {
			RunScript.execute(conn, new FileReader(fileName));
		} catch (SQLException |FileNotFoundException e) {
			log.error("populateTestData(): Error populating user data: ", e);
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void removeTestData(String fileName) {
		log.info("Removing Test Table and data ..... ");
		try(Connection conn = H2DAOFactory.getConnection();) {
			RunScript.execute(conn, new FileReader(fileName));
		} catch (SQLException |FileNotFoundException e) {
			log.error("removeTestData(): Error removing user data: ", e);
			throw new RuntimeException(e);
		}
	}
}
