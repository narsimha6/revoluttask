package com.fundtransfer.exception;

public class CustomException extends Exception {
	private Long code = null;
	public CustomException(Long code) {
		super();
		this.code = code;
	}
	public CustomException(Throwable cause, Long code) {
		super(cause);
		this.code = code;
	}
	public CustomException(String msg, Long code) {
		super(msg);
		this.code = code;
	}
	public CustomException(String msg, Throwable cause, Long code) {
		super(msg, cause);
		this.code = code;
	}
	public CustomException(String msg) {
		super(msg);
	}
	public Long getCode() {
		return code;
	}
}
