package com.fundtransfer.utils;

public final class FundTrnasferConstants {
	public final static String SQL_FILE_NAME = "src/test/resources/demo.sql";
	public final static String SQL_DELETE_FILE_NAME = "src/test/resources/removeTestData.sql";
	public final static String SQL_GET_ACC_BY_ID = "SELECT AccountId,UserName,Balance,CurrencyCode FROM Account WHERE AccountId = ? ";
	public final static String SQL_LOCK_ACC_BY_ID = "SELECT AccountId,UserName,Balance,CurrencyCode FROM Account WHERE AccountId = ? FOR UPDATE";
	public final static String SQL_UPDATE_ACC_BALANCE = "UPDATE Account SET Balance = ? WHERE AccountId = ? ";
	public final static String SQL_INSERT_BANK_TRANSACTION = "INSERT INTO BankTransaction(TxId,fromAccountId,toAccountId, Amount, TxType, CurrencyCode, Status, ErrorMsg, TxTime) "
			+ "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
	public final static String SQL_GET_TX_BY_ID = "SELECT Status,ErrorMsg FROM BankTransaction WHERE TxId = ? ";
	public final static String SQL_GET_ALL_ACC = "SELECT AccountId,UserName,Balance,CurrencyCode FROM Account";
	public final static String TX_FAIL = "FAIL";
	public final static String TX_SUCCESS = "SUCCESS";
	
	/*
	 * DB related properties loaded
	 */
	public static final String H2_DRIVER = Utils.getStringProperty("h2_driver");
	public static final String H2_CONNECTION_URL = Utils.getStringProperty("h2_connection_url");
	public static final String H2_USER = Utils.getStringProperty("h2_user");
	public static final String H2_PASSWORD = Utils.getStringProperty("h2_password");
}
