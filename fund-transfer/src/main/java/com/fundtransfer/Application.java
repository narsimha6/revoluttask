package com.fundtransfer;

import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;

import com.fundtransfer.dao.DAOFactory;
import com.fundtransfer.service.AccountService;
import com.fundtransfer.service.ServiceExceptionMapper;
import com.fundtransfer.service.TransactionService;
import com.fundtransfer.utils.FundTrnasferConstants;

/**
 * Main Class (Application starting point) 
 * @author Jillella Narasimha Rao
 */
public class Application {

	private static Logger log = Logger.getLogger(Application.class);

	public static void main(String[] args) throws Exception {
		// Initialize H2 database with demo data
		log.info("Initialize Database .....");
		DAOFactory.getDAOFactory().populateTestData(FundTrnasferConstants.SQL_FILE_NAME);
		log.info("Initialization Complete....");
		// Host service on jetty
		startService();
	}
	 
	/***
	 * To start the server and service
	 * @throws Exception
	 */
	private static void startService() throws Exception {
		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		context.setContextPath("/");
		Server server = new Server(8080);
		server.setHandler(context);
		ServletHolder servletHolder = context.addServlet(ServletContainer.class, "/*");
		servletHolder.setInitParameter("jersey.config.server.provider.classnames",
				 AccountService.class.getCanonicalName() + ","
						+ ServiceExceptionMapper.class.getCanonicalName() + ","
						+ TransactionService.class.getCanonicalName());
		try{
			server.start();
			server.join();
		} finally {
			server.destroy();
		}
	}
}