package com.fundtransfer.service;

import java.util.Objects;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fundtransfer.dao.DAOFactory;
import com.fundtransfer.exception.CustomException;
import com.fundtransfer.model.MoneyUtil;
import com.fundtransfer.model.UserTransaction; 
import com.fundtransfer.utils.FundTrnasferConstants;

@Path("/transaction")
@Produces(MediaType.APPLICATION_JSON)
public class TransactionService {

	private final DAOFactory daoFactory = DAOFactory.getDAOFactory();
	 
	/**
	 * Transfer fund between two accounts.
	 * @param transaction
	 * @return
	 * @throws CustomException
	 */
	@POST
	public Response transferFund(UserTransaction transaction) throws CustomException {
		if(Objects.isNull(transaction)) {
			throw new WebApplicationException("Transaction param can not be NULL", Response.Status.BAD_REQUEST);
		}
		String currency = transaction.getCurrencyCode();
		if (MoneyUtil.INSTANCE.validateCcyCode(currency)) {
			String txId = daoFactory.getAccountDAO().transferAccountBalance(transaction);
			if (!Objects.isNull(txId) && txId.contains(FundTrnasferConstants.TX_SUCCESS)) {
				return Response.ok(txId).build();
			} else {
				// transaction failed
				throw new WebApplicationException("Transaction failed, "+txId, Response.Status.BAD_REQUEST);
			}
		} else {
			throw new WebApplicationException("Currency Code Invalid ", Response.Status.BAD_REQUEST);
		}
	}
	
	@GET
	@Path("/{transactionId}")
    public String getTransactionById(@PathParam("transactionId") Long transactionId) throws CustomException {
        final String accTxDetails = daoFactory.getAccountDAO().getTransactionById(transactionId);

        if(accTxDetails == null){
            throw new WebApplicationException("Transaction not found", Response.Status.NOT_FOUND);
        }
        return accTxDetails;
    }
}